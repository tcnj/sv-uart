/* 
SystemVerilog UART Transmitter and Receiver cores
Copyright (C) 2020  Danny Wensley

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use common_api::*;
use uart_rx::*;

fn clock(model: &mut Model, time: &mut u64) {
    model.set(Settable::Input(Input::i_clk), 0); // Ensure clock is 0

    // Eval and trace at beginning to allow combinatorial logic to settle
    model.eval();
    model.dump_trace(*time + 3);
    
    // Clock positive edge
    model.set(Settable::Input(Input::i_clk), 1);

    // Eval and trace positive edge logic
    model.eval();
    model.dump_trace(*time + 5);

    // Clock negative edge
    model.set(Settable::Input(Input::i_clk), 0);

    // Eval and trace negative edge logic
    model.eval();
    model.dump_trace(*time + 10);

    // Update time
    *time += 10;
}

fn send_bit(model: &mut Model, time: &mut u64, value: bool) {
    model.set(Settable::Input(Input::i_line), value as u64);

    clock(model, time);
    clock(model, time);
    clock(model, time);
    clock(model, time);
    clock(model, time);
}

fn send_byte(model: &mut Model, time: &mut u64, mut value: u8) {
    send_bit(model, time, false);

    for _ in 0..8 {
        send_bit(model, time, value & 0x1 > 0);
        value >>= 1;
    }

    send_bit(model, time, true);
}

fn main() {
    println!("Starting...");
    let mut model = Model::new();
    model.open_trace("test.gcd", 99);
    model.set(Settable::Input(Input::i_line), 1);
    model.dump_trace(0);
    let mut time: u64 = 0;

    for _ in 0..10 {
        clock(&mut model, &mut time);
    }

    send_byte(&mut model, &mut time, 0b1010_0101);
    send_byte(&mut model, &mut time, 0b1010_1101);

    for _ in 0..10 {
        clock(&mut model, &mut time);
    }
}
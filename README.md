# SystemVerilog UART Transmitter and Receiver cores

These are some stupid simple cores for transmitting and receiving UART. Everything here is licensed under the GPL v3 (SPDX GPL-3.0-only). The full terms are in the LICENSE.md file.
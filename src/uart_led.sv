/* 
SystemVerilog UART Transmitter and Receiver cores
Copyright (C) 2020  Danny Wensley

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

`default_nettype none

module uart_led(
    i_clk,
    i_uart_rx,

    o_leds,
    o_uart_tx
);

input bit i_clk;

input bit i_uart_rx;
output bit o_uart_tx;

output bit [2:0] o_leds;

bit [7:0] uart_rx_data;
bit uart_rx_strobe;

localparam CLOCKS_PER_BIT = 434;

uart_rx #(
    .CLOCKS_PER_BIT(CLOCKS_PER_BIT)
) uart_rx (
    .i_clk(i_clk),
    .i_line(i_uart_rx),

    .o_data(uart_rx_data),
    .o_strobe(uart_rx_strobe)
);

uart_tx #(
    .CLOCKS_PER_BIT(CLOCKS_PER_BIT)
) uart_tx (
    .i_clk,
    .i_data(uart_rx_data),
    .i_data_strobe(uart_rx_strobe),
    .o_line(o_uart_tx)
);

always_ff @(posedge i_clk)
    if (uart_rx_strobe)
        case (uart_rx_data)
            8'h31: o_leds[0] <= !o_leds[0];
            8'h32: o_leds[1] <= !o_leds[1];
            8'h33: o_leds[2] <= !o_leds[2];
            default: o_leds <= 3'b000;
        endcase

endmodule

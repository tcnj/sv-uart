/* 
SystemVerilog UART Transmitter and Receiver cores
Copyright (C) 2020  Danny Wensley

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use common_api::*;
use uart_tx::*;

fn clock(model: &mut Model, time: &mut u64) {
    model.set(Settable::Input(Input::i_clk), 0); // Ensure clock is 0

    // Eval and trace at beginning to allow combinatorial logic to settle
    model.eval();
    model.dump_trace(*time + 3);
    
    // Clock positive edge
    model.set(Settable::Input(Input::i_clk), 1);

    // Eval and trace positive edge logic
    model.eval();
    model.dump_trace(*time + 5);

    // Clock negative edge
    model.set(Settable::Input(Input::i_clk), 0);

    // Eval and trace negative edge logic
    model.eval();
    model.dump_trace(*time + 10);

    // Update time
    *time += 10;
}

fn main() {
    println!("Starting...");
    let mut model = Model::new();
    model.open_trace("test.gcd", 99);
    model.dump_trace(0);
    let mut time: u64 = 0;

    for _ in 0..10 {
        clock(&mut model, &mut time);
    }

    model.set(Settable::Input(Input::i_data), 0b1010_1101);
    model.set(Settable::Input(Input::i_data_strobe), 1);
    clock(&mut model, &mut time);
    model.set(Settable::Input(Input::i_data_strobe), 0);

    for _ in 0..100 {
        clock(&mut model, &mut time);
    }
}
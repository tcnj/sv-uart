/* 
SystemVerilog UART Transmitter and Receiver cores
Copyright (C) 2020  Danny Wensley

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

`default_nettype none

module uart_rx #(
    parameter CLOCKS_PER_BIT = 5
)(
    i_clk, i_line, o_data, o_strobe
);

input bit i_clk;
    
input bit i_line;

output bit [7:0] o_data;
output bit o_strobe;

bit [$clog2(CLOCKS_PER_BIT):0] clock_wait_timer = 0;

bit [2:0] bits_left = 0;
bit [1:0] state = 0;
bit [7:0] data = 0;

localparam IDLE = 2'b00;
localparam GOT_START = 2'b01;
localparam GOT_DATA = 2'b10;
localparam BREAK = 2'b11;

always_ff @(posedge i_clk)
    if (clock_wait_timer == CLOCKS_PER_BIT - 1) clock_wait_timer <= 0;
    else clock_wait_timer <= clock_wait_timer + 1;

always_ff @(posedge i_clk) begin
    if (clock_wait_timer == 0)
        case (state)
            IDLE : begin
                if (i_line == 0) state <= GOT_START;
            end
            GOT_START : begin
                data <= {i_line, 7'b0};
                state <= GOT_DATA;
                bits_left <= 7;
            end
            GOT_DATA : begin
                if (bits_left == 0) begin
                    if (i_line) begin
                        state <= IDLE;
                        o_data <= data;
                        o_strobe <= 1;
                    end else state <= BREAK;
                end else begin
                   data <= {i_line, data[7:1]};
                   bits_left <= bits_left - 1;
                end
            end
            BREAK : begin
                if (i_line) state <= IDLE;
            end
        endcase

    if (state == IDLE) o_strobe <= 0;
end

endmodule

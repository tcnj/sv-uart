/* 
SystemVerilog UART Transmitter and Receiver cores
Copyright (C) 2020  Danny Wensley

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

`default_nettype none

module uart_tx #(
    parameter CLOCKS_PER_BIT = 5
)(
    i_clk, i_data, i_data_strobe,
    o_line, o_busy
);

input bit i_clk;

input bit [7:0] i_data;
input bit i_data_strobe;

output bit o_line = 1;
output bit o_busy;

bit data_waiting = 0;
bit [7:0] send_buffer = 0;
bit [2:0] bit_index = 0;

bit [1:0] state;
localparam IDLE = 2'b00;
localparam SEND_BITS = 2'b01;
localparam SEND_STOP = 2'b10;

bit [$clog2(CLOCKS_PER_BIT):0] clock_wait_timer = 0;

always_ff @(posedge i_clk)
    if (clock_wait_timer == CLOCKS_PER_BIT - 1) clock_wait_timer <= 0;
    else clock_wait_timer <= clock_wait_timer + 1;

always_ff @(posedge i_clk) begin
    if (!o_busy && i_data_strobe) begin
        send_buffer <= i_data;
        data_waiting <= 1;
        o_busy <= 1;
    end

    if (clock_wait_timer == 0) begin
        case (state)
            IDLE: if (data_waiting) begin
                o_line <= 0;
                state <= SEND_BITS;
                bit_index <= 0;
                data_waiting <= 0;
            end
            SEND_BITS: begin
                o_line <= send_buffer[0];
                send_buffer <= {1'b0, send_buffer[7:1]};
                bit_index <= bit_index + 1;

                if (bit_index == 7) state <= SEND_STOP;
            end
            default: begin
                state <= IDLE;
                o_line <= 1;
                o_busy <= 0;
            end
        endcase
    end
end

endmodule
